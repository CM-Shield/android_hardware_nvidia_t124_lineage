// SPDX-License-Identifier: GPL-2.0
/dts-v1/;

#include <dt-bindings/input/input.h>
#include "tegra124-lineage.dtsi"
#include "tegra124-ardbeg-pinmux.dtsi"

/ {
	model = "NVIDIA Tegra124 Ardbeg";
	compatible = "nvidia,ardbeg", "nvidia,tegra124";

	aliases {
		rtc0 = "/i2c@7000d000/pmic@58";
		rtc1 = "/rtc@7000e000";

		serial0 = &uartd;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};

	firmware {
		android {
			compatible = "android,firmware";
			hardware = "ardbeg";
			boot_devices = "soc0/700b0600.mmc";
		};
	};

	memory@80000000 {
		reg = <0x0 0x80000000 0x0 0x80000000>;
	};

	host1x@50000000 {
		hdmi@54280000 {
			status = "okay";

			hdmi-supply = <&palmas_smps9_reg>;
			pll-supply = <&palmas_smps9_reg>;
			vdd-supply = <&vdd_5v0_hdmi>;

			nvidia,ddc-i2c-bus = <&hdmi_ddc>;
			nvidia,hpd-gpio =
				<&gpio TEGRA_GPIO(N, 7) GPIO_ACTIVE_HIGH>;
		};

		dsi@54300000 {
			status = "disabled";

			avdd-dsi-csi-supply = <&ldo4_reg>;

			panel@0 {
				compatible = "panasonic,vvx10f004b00";
				reg = <0>;

				power-supply = <&vdd_lcd_reg>;
				backlight = <&backlight>;
			};
		};
	};

	gpu@0,57000000 {
		status = "okay";
		vdd-supply = <&vdd_gpu>;
	};

	/* Fourth high speed UART, exposed via debug board usb */
	serial@70006300 {
		status = "okay";
	};

	/* Expansion GEN1_I2C_*, on-board components */
	i2c@7000c000 {
		status = "okay";
		clock-frequency = <100000>;
	};

	pwm@7000a000 {
		status = "okay";
	};

	/* Expansion GEN2_I2C_* */
	i2c@7000c400 {
		status = "okay";
		clock-frequency = <100000>;
	};

	/* Expansion CAM_I2C_* */
	i2c@7000c500 {
		status = "okay";
		clock-frequency = <100000>;
	};

	/* HDMI DDC */
	hdmi_ddc: i2c@7000c700 {
		status = "okay";
		clock-frequency = <100000>;
	};

	/* Expansion PWR_I2C_*, on-board components */
	i2c@7000d000 {
		status = "okay";
		clock-frequency = <400000>;

		palmas: tps65913@58 {
			compatible = "ti,tps65913","ti,palmas";
			reg = <0x58>;
			interrupts = <GIC_SPI 86 IRQ_TYPE_LEVEL_HIGH>;

			#interrupt-cells = <2>;
			interrupt-controller;

			ti,system-power-controller;

			palmas_gpio: gpio {
				compatible = "ti,palmas-gpio";
				gpio-controller;
				#gpio-cells = <2>;
			};

			extcon {
				compatible = "ti,palmas-usb-vid";
				ti,wakeup;
				ti,enable-id-detection;
				ti,enable-vbus-detection;
			};

			palmas_clk32k@0 {
				compatible = "ti,palmas-clk32kg";
				#clock-cells = <0>;
			};

			palmas_clk32k@1 {
				compatible = "ti,palmas-clk32kgaudio";
				#clock-cells = <0>;
			};

			pmic {
				compatible = "ti,tps65913-pmic", "ti,palmas-pmic";
				ldo1-in-supply = <&palmas_smps7_reg>;
				ldo2-in-supply = <&vdd_1v8>;
				ldo4-in-supply = <&vdd_1v8>;
				ldoln-in-supply = <&vdd_1v8>;

				regulators {
					vdd_gpu: smps123 {
						regulator-name = "vdd-gpu";
						regulator-min-microvolt = <650000>;
						regulator-max-microvolt = <1400000>;
						regulator-init-microvolt = <900000>;
						regulator-boot-on;
					};

					vdd_core: smps45 {
						regulator-name = "vdd-core";
						regulator-min-microvolt = <700000>;
						regulator-max-microvolt = <1400000>;
						regulator-always-on;
						regulator-boot-on;
					};

					vdd_1v8: smps6 {
						regulator-name = "vdd-1v8";
						regulator-always-on;
						regulator-boot-on;
					};

					palmas_smps7_reg: smps7 {
						regulator-name = "vddio-ddr";
						regulator-always-on;
						regulator-boot-on;
					};

					palmas_smps9_reg: smps9 {
						regulator-name = "pex-hdmi";
					};

					ldo1_reg: ldo1 {
						regulator-name = "avdd-pll";
						regulator-always-on;
						regulator-boot-on;
					};

					vdd_1v2_ap: ldo2 {
						regulator-name = "vdd-1v2";
						regulator-min-microvolt = <1200000>;
						regulator-max-microvolt = <1200000>;
					};

					ldo3 {
						regulator-name = "vdd-touch";
						regulator-min-microvolt = <3100000>;
						regulator-max-microvolt = <3100000>;
					};

					ldo4_reg: ldo4 {
						regulator-name = "avdd-hsic";
					};

					ldo5 {
						regulator-name = "vdd-2v7";
						regulator-min-microvolt = <2700000>;
						regulator-max-microvolt = <2700000>;
					};

					ldo6 {
						regulator-name = "vdd-cam-af";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
						regulator-always-on;
						regulator-boot-on;
					};

					ldo7 {
						regulator-name = "avdd-cam-af";
						regulator-min-microvolt = <2700000>;
						regulator-max-microvolt = <2700000>;
					};

					ldo8 {
						regulator-name = "vdd-rtc";
						regulator-min-microvolt = <800000>;
						regulator-max-microvolt = <800000>;
						regulator-always-on;
						regulator-boot-on;
						ti,enable-ldo8-tracking;
					};

					ldo9 {
						regulator-name = "vddio-sdmmc-2";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <3300000>;
					};

					ldoln {
						regulator-name = "vdd-cam-1v05";
						regulator-min-microvolt = <1050000>;
						regulator-max-microvolt = <1050000>;
					};

					ldousb_reg: ldousb {
						regulator-name = "vpp-fuse";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
						regulator-always-on;
						regulator-boot-on;
					};

					regen1: regen1 {
						regulator-name = "rail-3v3";
						regulator-max-microvolt = <3300000>;
						regulator-always-on;
						regulator-boot-on;
					};
				};
			};

			rtc {
				compatible = "ti,palmas-rtc";
				interrupt-parent = <&palmas>;
				interrupts = <8 0>;
			};

			pinmux {
				compatible = "ti,tps65913-pinctrl";
				pinctrl-names = "default";
				pinctrl-0 = <&palmas_default>;
				ti,palmas-enable-dvfs1;

				palmas_default: pinmux {
					powergood {
						pins = "powergood";
						function = "powergood";
					};

					vac {
						pins = "vac";
						function = "vac";
					};

					pin_gpio0 {
						pins = "gpio0";
						function = "id";
						bias-pull-up;
					};

					pin_gpio1 {
						pins = "gpio1";
						function = "vbus_det";
					};

					pin_gpio2 {
						pins = "gpio2";
						function = "gpio";
					};

					pin_gpio3 {
						pins = "gpio3";
						function = "gpio";
					};

					pin_gpio4 {
						pins = "gpio4";
						function = "gpio";
					};

					pin_gpio5 {
						pins = "gpio5";
						function = "clk32kgaudio";
					};

					pin_gpio6 {
						pins = "gpio6";
						function = "gpio";
					};

					pin_gpio7 {
						pins = "gpio7";
						function = "gpio";
					};
				};
			};
		};
	};

	/* Expansion TS_SPI_* */
	spi@7000d400 {
		status = "okay";
		spi-max-frequency = <25000000>;
	};

	/* Internal SPI */
	spi@7000da00 {
		status = "okay";
		spi-max-frequency = <25000000>;
	};

	pmc@7000e400 {
		nvidia,invert-interrupt;
		nvidia,suspend-mode = <0>;
		nvidia,cpu-pwr-good-time = <500>;
		nvidia,cpu-pwr-off-time = <300>;
		nvidia,core-pwr-good-time = <3845 3845>;
		nvidia,core-pwr-off-time = <2000>;
		nvidia,core-power-req-active-high;
		nvidia,sys-clock-req-active-high;
	};

	hda@70030000 {
		status = "okay";
	};

	usb@70090000 {
		phys = <&{/padctl@7009f000/pads/usb2/lanes/usb2-0}>, /* Micro A/B */
		       <&{/padctl@7009f000/pads/pcie/lanes/pcie-0}>, /* Micro A/B (SS) */
		       <&{/padctl@7009f000/pads/usb2/lanes/usb2-1}>, /* ?? */
		       <&{/padctl@7009f000/pads/usb2/lanes/usb2-2}>, /* Host */
		       <&{/padctl@7009f000/pads/pcie/lanes/pcie-1}>; /* Host (SS) */
		phy-names = "usb2-0", "usb3-0", "usb2-1", "usb2-2", "usb3-1";

		avddio-pex-supply = <&palmas_smps9_reg>;
		dvddio-pex-supply = <&palmas_smps9_reg>;
		avdd-usb-supply = <&regen1>;
		avdd-pll-utmip-supply = <&vdd_1v8>;
		avdd-pll-erefe-supply = <&ldo4_reg>;
		avdd-usb-ss-pll-supply = <&palmas_smps9_reg>;
		hvdd-usb-ss-supply = <&regen1>;
		hvdd-usb-ss-pll-e-supply = <&regen1>;

		status = "okay";
	};

	padctl@7009f000 {
		status = "okay";

		avdd-pll-utmip-supply = <&vdd_1v8>;
		avdd-pll-erefe-supply = <&ldo4_reg>;
		avdd-pex-pll-supply = <&palmas_smps9_reg>;
		hvdd-pex-pll-e-supply = <&regen1>;

		pads {
			usb2 {
				status = "okay";

				lanes {
					usb2-0 {
						status = "okay";
						nvidia,function = "snps";
					};

					usb2-1 {
						status = "okay";
						nvidia,function = "xusb";
					};

					usb2-2 {
						status = "okay";
						nvidia,function = "xusb";
					};
				};
			};

			pcie {
				status = "okay";

				lanes {
					pcie-0 {
						status = "okay";
						nvidia,function = "usb3-ss";
					};

					pcie-1 {
						status = "okay";
						nvidia,function = "usb3-ss";
					};
				};
			};
		};

		ports {
			/* Micro A/B */
			usb2-0 {
				status = "okay";
				mode = "otg";
				usb-role-switch;
				vbus-supply = <&vdd_usb0_vbus>;
			};

			/* ?? */
			usb2-1 {
				status = "okay";
				mode = "host";
				vbus-supply = <&vdd_usb1_vbus>;
			};

			/* Host */
			usb2-2 {
				status = "okay";
				mode = "host";
				vbus-supply = <&vdd_usb2_vbus>;
			};

			usb3-0 {
				status = "disabled";
				nvidia,usb2-companion = <0>;
				vbus-supply = <&vdd_usb0_vbus>;
			};

			usb3-1 {
				status = "okay";
				nvidia,usb2-companion = <2>;
				vbus-supply = <&vdd_usb2_vbus>;
			};
		};
	};

	/* SD card */
	mmc@700b0400 {
		status = "okay";
		cd-gpios = <&gpio TEGRA_GPIO(V, 2) GPIO_ACTIVE_LOW>;
		power-gpios = <&gpio TEGRA_GPIO(R, 0) GPIO_ACTIVE_HIGH>;
		wp-gpios = <&gpio TEGRA_GPIO(Q, 4) GPIO_ACTIVE_HIGH>;
		bus-width = <4>;
		vqmmc-supply = <&vdd_1v8>;
	};

	/* eMMC */
	mmc@700b0600 {
		status = "okay";
		bus-width = <8>;
		non-removable;
	};

	/* CPU DFLL clock */
	clock@70110000 {
		status = "okay";
		vdd-cpu-supply = <&vdd_core>;
		nvidia,i2c-fs-rate = <400000>;
	};

	ahub@70300000 {
		i2s@70301100 {
			status = "okay";
		};
	};

	usb@7d000000 {
		compatible = "nvidia,tegra124-udc";
		status = "okay";
		dr_mode = "peripheral";
	};

	usb-phy@7d000000 {
		status = "okay";
	};

	backlight: backlight {
		compatible = "pwm-backlight";

		enable-gpios = <&gpio TEGRA_GPIO(H, 1) GPIO_ACTIVE_HIGH>;
		power-supply = <&lcd_bl_en>;
		pwms = <&pwm 1 1000000>;

		brightness-levels = <0 4 8 16 32 64 128 255>;
		default-brightness-level = <6>;
	};

	clk32k_in: clock-32k {
		compatible = "fixed-clock";
		clock-frequency = <32768>;
		#clock-cells = <0>;
	};

	cpus {
		cpu@0 {
			vdd-cpu-supply = <&vdd_core>;
			enable-method = "psci";
		};

		cpu@1 {
			enable-method = "psci";
		};

		cpu@2 {
			enable-method = "psci";
		};

		cpu@3 {
			enable-method = "psci";
		};
	};

	gpio-keys {
		compatible = "gpio-keys";

		power {
			label = "Power";
			gpios = <&gpio TEGRA_GPIO(Q, 0) GPIO_ACTIVE_LOW>;
			linux,code = <KEY_POWER>;
			debounce-interval = <10>;
			wakeup-source;
		};
	};

	psci {
		compatible = "arm,psci-0.2";
		method = "smc";
	};

	vdd_ac_bat: regulator@0 {
		compatible = "regulator-fixed";
		regulator-name = "vdd_ac_bat";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
	};

	vdd_5v0_hdmi: regulator@1 {
		compatible = "regulator-fixed";
		regulator-name = "vdd-hdmi-5v0";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&gpio TEGRA_GPIO(K, 6) GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	vdd_usb0_vbus: regulator@2 {
		compatible = "regulator-fixed";
		regulator-name = "+USB0_VBUS_SW";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&gpio TEGRA_GPIO(N, 4) GPIO_ACTIVE_HIGH>;
		enable-active-high;
		gpio-open-drain;
	};

	vdd_usb1_vbus: regulator@3 {
		compatible = "regulator-fixed";
		regulator-name = "+5V_USB_HS";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&gpio TEGRA_GPIO(N, 5) GPIO_ACTIVE_HIGH>;
		enable-active-high;
		gpio-open-drain;
	};

	vdd_usb2_vbus: regulator@4 {
		compatible = "regulator-fixed";
		regulator-name = "+USB2_VBUS_SW";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&gpio TEGRA_GPIO(FF, 1) GPIO_ACTIVE_HIGH>;
		enable-active-high;
		gpio-open-drain;
	};

	lcd_bl_en: regulator@5 {
		compatible = "regulator-fixed";
		regulator-name = "lcd_bl_en";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		enable-active-high;
	};

	dcdc_1v8: regulator@6 {
		compatible = "regulator-fixed";
		regulator-name = "dcdc-1v8";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
	};

	vdd_5v0_sys: regulator@7 {
		compatible = "regulator-fixed";
		regulator-name = "+5V_SYS";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

	vdd_lcd_reg: regulator@13 {
		compatible = "regulator-fixed";
		regulator-name = "vdd-lcd";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		gpio = <&palmas_gpio 7 0>;
	};

	vdd_cpu_fixed: regulator@14 {
		compatible = "regulator-fixed";
		regulator-name = "vdd-cpu-fixed";
		regulator-min-microvolt = <1000000>;
		regulator-max-microvolt = <1000000>;
		regulator-boot-on;
	};
};
