// SPDX-License-Identifier: GPL-2.0
/dts-v1/;

#include <dt-bindings/input/input.h>
#include "tegra124-lineage.dtsi"
#include "tegra124-loki-pinmux.dtsi"

/ {
	model = "NVIDIA SHIELD Portable";
	compatible = "nvidia,loki", "nvidia,tegra124";

	aliases {
		rtc0 = "/i2c@0,7000d000/pmic@58";
		rtc1 = "/rtc@0,7000e000";
		serial0 = &uartd;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};

	firmware {
		android {
			compatible = "android,firmware";
			hardware = "loki";
			boot_devices = "soc0/700b0600.mmc";
		};
	};

	memory {
		reg = <0x0 0x80000000 0x0 0x2d000000
		       0x0 0xae000000 0x0 0x30000000>;
	};

	host1x@50000000 {
		hdmi@54280000 {
			status = "okay";

			hdmi-supply = <&vdd_5v0_hdmi>;
			pll-supply = <&tps65913_ldo1>;
			vdd-supply = <&ldousb_reg>;

			nvidia,ddc-i2c-bus = <&hdmi_ddc>;
			nvidia,hpd-gpio =
				<&gpio TEGRA_GPIO(N, 7) GPIO_ACTIVE_HIGH>;
		};

		dsi@54300000 {
			status = "okay";

			avdd-dsi-csi-supply = <&tps65913_ldo3>;

			panel@0 {
				compatible = "jdi,58-1440-810";
				reg = <0>;

				enable-gpios = <&gpio TEGRA_GPIO(Q, 2) GPIO_ACTIVE_HIGH>;
				reset-gpios = <&gpio TEGRA_GPIO(H, 3) GPIO_ACTIVE_LOW>;

				avdd_lcd-supply = <&tps65913_ldo2>;
				dvdd_lcd-supply = <&dvdd_lcd>;
				backlight = <&backlight>;
			};
		};
	};

	cec@70015000 {
		status = "okay";
	};

	gpu@57000000 {
		status = "okay";
		vdd-supply = <&vdd_gpu>;
	};

	/* Fourth high speed UART, exposed via debug board usb */
	serial@70006300 {
		status = "okay";
	};

	pwm@7000a000 {
		status = "okay";
	};

	i2c@7000c000 {
		status = "okay";
		clock-frequency = <100000>;

		/*rt5639: audio-codec@1c {
			compatible = "realtek,rt5639";
			reg = <0x1c>;
			interrupt-parent = <&gpio>;
			interrupts = <TEGRA_GPIO(H, 4) IRQ_TYPE_EDGE_FALLING>;
			realtek,ldo1-en-gpios =
				<&gpio TEGRA_GPIO(R, 2) GPIO_ACTIVE_HIGH>;
		};*/
	};

	/* Expansion GEN2_I2C_* */
	i2c@7000c400 {
		status = "okay";
		clock-frequency = <100000>;

		bat: battery-gauge@55 {
			compatible = "ti,bq27441";
			reg = <0x55>;
			power-supplies = <&charger>;

			interrupt-parent = <&gpio>;
			interrupts = <TEGRA_GPIO(Q, 5) IRQ_TYPE_NONE>;

			status = "okay";
		};

		charger: charger@6b {
			compatible = "ti,bq24190";
			reg = <0x6b>;

			interrupt-parent = <&gpio>;
			interrupts = <TEGRA_GPIO(J, 0) IRQ_TYPE_NONE>;
			omit-battery-class;

			battery_charger: charger {
				regulator-name = "batt_regulator";
			};
		};
	};

	/* HDMI DDC */
	hdmi_ddc: i2c@7000c700 {
		status = "okay";
		clock-frequency = <100000>;
	};

	/* Expansion PWR_I2C_*, on-board components */
	i2c@7000d000 {
		status = "okay";
		clock-frequency = <400000>;

		palmas: pmic@58 {
			compatible = "ti,tps65913", "ti,palmas";
			reg = <0x58>;
			interrupts = <GIC_SPI 86 IRQ_TYPE_LEVEL_HIGH>;

			#interrupt-cells = <2>;
			interrupt-controller;

			ti,system-power-controller;

			palmas_gpio: gpio {
				compatible = "ti,palmas-gpio";
				gpio-controller;
				#gpio-cells = <2>;
			};

			extcon {
				compatible = "ti,palmas-usb-vid";
				ti,wakeup;
				ti,enable-id-detection;
				ti,enable-vbus-detection;
			};

			palmas_clk32k@0 {
				compatible = "ti,palmas-clk32kg";
				#clock-cells = <0>;
			};

			palmas_clk32k@1 {
				compatible = "ti,palmas-clk32kgaudio";
				#clock-cells = <0>;
			};

			pmic {
				compatible = "ti,tps65913-pmic", "ti,palmas-pmic";
				ldo1-in-supply = <&palmas_smps8_reg>;
				ldo2-in-supply = <&palmas_smps6_reg>;
				ldo3-in-supply = <&palmas_smps8_reg>;
				ldo4-in-supply = <&palmas_smps6_reg>;
				ldo5-in-supply = <&palmas_smps8_reg>;
				ldo6-in-supply = <&palmas_smps6_reg>;
				ldo9-in-supply = <&palmas_smps6_reg>;

				regulators {
					vdd_gpu: smps123 {
						regulator-name = "vdd-gpu";
						regulator-min-microvolt = <650000>;
						regulator-max-microvolt = <1400000>;
						regulator-init-microvolt = <1000000>;
						regulator-boot-on;
					};

					vdd_core: smps45 {
						regulator-name = "vdd-core";
						regulator-min-microvolt = <700000>;
						regulator-max-microvolt = <1250000>;
						regulator-ramp-delay = <5000>;
						regulator-always-on;
						regulator-boot-on;
					};

					palmas_smps6_reg: smps6 {
						regulator-name = "vdd-3v3";
						regulator-min-microvolt = <3300000>;
						regulator-max-microvolt = <3300000>;
						regulator-always-on;
						regulator-boot-on;
					};

					smps7 {
						regulator-name = "vddio-ddr";
						regulator-min-microvolt = <1500000>;
						regulator-max-microvolt = <1500000>;
						regulator-always-on;
						regulator-boot-on;
					};

					palmas_smps8_reg: smps8 {
						regulator-name = "vdd-1v8";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
						regulator-always-on;
						regulator-boot-on;
					};

					palmas_smps9_reg: smps9 {
						regulator-name = "vddio-sdslot";
						regulator-min-microvolt = <2800000>;
						regulator-max-microvolt = <2800000>;
						regulator-ramp-delay = <250>;
					};

					tps65913_smps10_out1: smps10_out1 {
						regulator-name = "vdd-cam-5v0";
						regulator-min-microvolt = <5000000>;
						regulator-max-microvolt = <5000000>;
						regulator-always-on;
						regulator-boot-on;
					};

					tps65913_ldo1: ldo1 {
						regulator-name = "avdd-pll";
						regulator-min-microvolt = <1050000>;
						regulator-max-microvolt = <1050000>;
						regulator-boot-on;
					};

					tps65913_ldo2: ldo2 {
						regulator-name = "avdd-lcd";
						regulator-min-microvolt = <2800000>;
						regulator-max-microvolt = <3100000>;
						regulator-boot-on;
					};

					tps65913_ldo3: ldo3 {
						regulator-name = "avdd_dsi_csi";
						regulator-min-microvolt = <1200000>;
						regulator-max-microvolt = <1200000>;
						regulator-boot-on;
					};

					ldo4 {
						regulator-name = "vpp-fuse";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					ldo5 {
						regulator-name = "vddio-ddr-hs";
						regulator-min-microvolt = <1100000>;
						regulator-max-microvolt = <1100000>;
						regulator-always-on;
						regulator-boot-on;
					};

					ldo6 {
						regulator-name = "vdd-sensor";
						regulator-min-microvolt = <2850000>;
						regulator-max-microvolt = <2850000>;
					};

					ldo8 {
						regulator-name = "vdd-cpu";
						regulator-min-microvolt = <900000>;
						regulator-max-microvolt = <900000>;
						regulator-always-on;
						regulator-boot-on;
					};

					ldo9 {
						regulator-name = "vddio-sdmmc3";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <3300000>;
					};

					ldoln_reg: ldoln {
						regulator-name = "vdd-ldoln";
						regulator-min-microvolt = <2800000>;
						regulator-max-microvolt = <3300000>;
					};

					ldousb_reg: ldousb {
						regulator-name = "vddio-hv";
						regulator-min-microvolt = <2300000>;
						regulator-max-microvolt = <3300000>;
					};

					regen1 {
						regulator-name = "vdd-regen1";
						regulator-max-microvolt = <3300000>;
						regulator-always-on;
						regulator-boot-on;
					};

					regen2 {
						regulator-name = "vdd-regen2";
						regulator-max-microvolt = <5000000>;
						regulator-always-on;
						regulator-boot-on;
					};
				};
			};

			rtc {
				compatible = "ti,palmas-rtc";
				interrupt-parent = <&palmas>;
				interrupts = <8 0>;
			};

			pinmux {
				compatible = "ti,tps65913-pinctrl";
				pinctrl-names = "default";
				pinctrl-0 = <&palmas_default>;
				ti,palmas-enable-dvfs1;

				palmas_default: pinmux {
					powergood {
						pins = "powergood";
						function = "powergood";
					};

					vac {
						pins = "vac";
						function = "vac";
					};

					pin_gpio0 {
						pins = "gpio0";
						function = "id";
						bias-pull-up;
					};

					pin_gpio1 {
						pins = "gpio1";
						function = "gpio";
					};

					pin_gpio2 {
						pins = "gpio2";
						function = "gpio";
					};

					pin_gpio3 {
						pins = "gpio3";
						function = "gpio";
					};

					pin_gpio4 {
						pins = "gpio4";
						function = "gpio";
					};

					pin_gpio5 {
						pins = "gpio5";
						function = "clk32kgaudio";
					};

					pin_gpio6 {
						pins = "gpio6";
						function = "gpio";
					};

					pin_gpio7 {
						pins = "gpio7";
						function = "gpio";
					};
				};
			};
		};
	};

	hda@70030000 {
		status = "okay";
	};

	usb@70090000 {
		phys = <&{/padctl@7009f000/pads/usb2/lanes/usb2-0}>; /* Micro A/B */
		       //<&{/padctl@7009f000/pads/usb2/lanes/usb2-1}>, /* Internal USB */
		       //<&{/padctl@7009f000/pads/pcie/lanes/pcie-0}>, /* Micro A/B (SS) */
		       //<&{/padctl@7009f000/pads/pcie/lanes/pcie-1}>; /* Internal USB (SS) */
		phy-names = "usb2-0", "usb2-1";

		avddio-pex-supply = <&tps65913_ldo1>;
		dvddio-pex-supply = <&tps65913_ldo1>;
		avdd-usb-supply = <&ldousb_reg>;
		avdd-pll-utmip-supply = <&palmas_smps8_reg>;
		avdd-pll-erefe-supply = <&tps65913_ldo3>;
		avdd-usb-ss-pll-supply = <&tps65913_ldo1>;
		hvdd-usb-ss-supply = <&ldousb_reg>;
		hvdd-usb-ss-pll-e-supply = <&ldousb_reg>;

		status = "okay";
	};

	padctl@7009f000 {
		status = "okay";

		avdd-pll-utmip-supply = <&palmas_smps8_reg>;
		avdd-pll-erefe-supply = <&tps65913_ldo3>;
		avdd-pex-pll-supply = <&tps65913_ldo1>;
		hvdd-pex-pll-e-supply = <&ldousb_reg>;

		pads {
			usb2 {
				status = "okay";

				lanes {
					usb2-0 {
						status = "okay";
						nvidia,function = "xusb";
					};

					/*usb2-1 {
						nvidia,function = "xusb";
						status = "okay";
					};*/
				};
			};

			/*pcie {
				status = "okay";

				lanes {
					pcie-0 {
						status = "okay";
						nvidia,function = "usb3-ss";
					};

					pcie-1 {
						status = "okay";
						nvidia,function = "usb3-ss";
					};
				};
			};*/
		};

		ports {
			/* Micro A/B */
			usb2-0 {
				status = "okay";
				mode = "otg";
				usb-role-switch;
				vbus-supply = <&vdd_ac_bat_reg>;
			};

			/* Internal USB */
			/*usb2-1 {
				status = "okay";
				mode = "host";
				vbus-supply = <&vdd_ac_bat_reg>;
			};

			usb3-0 {
				status = "okay";
				nvidia,usb2-companion = <0>;
			};

			usb3-1 {
				status = "okay";
				nvidia,usb2-companion = <1>;
			};*/
		};
	};

	/* SD card */
	mmc@700b0400 {
	};

	/* eMMC */
	mmc@700b0600 {
		status = "okay";
		bus-width = <8>;
		non-removable;
		vmmc-supply = <&palmas_smps9_reg>;
		vqmmc-supply = <&palmas_smps8_reg>;
	};

	/* CPU DFLL clock */
	clock@70110000 {
		status = "okay";
		vdd-cpu-supply = <&vdd_core>;
		nvidia,i2c-fs-rate = <400000>;
	};

	usb@7d000000 {
		compatible = "nvidia,tegra124-udc";
		status = "okay";
		dr_mode = "peripheral";
	};

	usb-phy@7d000000 {
		status = "okay";
	};

	backlight: backlight {
		compatible = "pwm-backlight";
		pwms = <&pwm 1 29334>;

		brightness-levels = <0 4 8 16 32 64 128 255>;
		default-brightness-level = <6>;

		power-supply = <&lcd_bl_en>;
		enable-gpios = <&gpio TEGRA_GPIO(H, 1) GPIO_ACTIVE_LOW>;
	};

	clk32k_in: clock-32k {
		compatible = "fixed-clock";
		clock-frequency = <32768>;
		#clock-cells = <0>;
	};

	cpus {
		cpu@0 {
			vdd-cpu-supply = <&vdd_core>;
			enable-method = "psci";
		};

		cpu@1 {
			enable-method = "psci";
		};

		cpu@2 {
			enable-method = "psci";
		};

		cpu@3 {
			enable-method = "psci";
		};
	};

	gpio-keys {
		compatible = "gpio-keys";

		key-power {
			label = "Power";
			gpios = <&gpio TEGRA_GPIO(Q, 0) GPIO_ACTIVE_LOW>;
			linux,code = <KEY_POWER>;
			debounce-interval = <10>;
			wakeup-source;
		};
	};

	psci {
		compatible = "arm,psci-0.2";
		method = "smc";
	};

	vdd_ac_bat_reg: regulator-acbat {
		compatible = "regulator-fixed";
		regulator-name = "vdd_ac_bat";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
	};

	lcd_bl_en: regulator-lcden {
		compatible = "regulator-fixed";
		regulator-name = "lcd_bl_en";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-boot-on;
		gpio = <&gpio TEGRA_GPIO(H, 2) GPIO_ACTIVE_HIGH>;
		enable-active-high;
		startup-delay-us = <1000>;
	};

	vdd_5v0_hdmi: regulator-hdmi {
		compatible = "regulator-fixed";
		regulator-name = "vdd_5v0_hdmi";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&gpio TEGRA_GPIO(FF, 0) GPIO_ACTIVE_HIGH>;
		enable-active-high;
		vin-supply = <&palmas_smps6_reg>;
	};

	dvdd_lcd: regulator-disp {
		compatible = "regulator-fixed";
		regulator-name = "display-1v8";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
	};

	sound {
	};

	thermal-zones {
	};
};
