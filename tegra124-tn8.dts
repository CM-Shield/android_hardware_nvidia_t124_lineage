// SPDX-License-Identifier: GPL-2.0
/dts-v1/;

#include <dt-bindings/input/input.h>
#include "tegra124-lineage.dtsi"
#include "tegra124-tn8-pinmux.dtsi"

/ {
	model = "NVIDIA SHIELD Tablet";
	compatible = "nvidia,tn8", "nvidia,tegra124";

	aliases {
		rtc0 = "/i2c@0,7000d000/pmic@58";
		rtc1 = "/rtc@0,7000e000";
		serial0 = &uartd;
	};

	chosen {
		stdout-path = "serial0:115200n8";
		bootargs = "fbcon=rotate:2";
	};

	firmware {
		android {
			compatible = "android,firmware";
			hardware = "tn8";
			boot_devices = "soc0/700b0600.mmc";
		};
	};

	memory {
		reg = <0x0 0x80000000 0x0 0x2d000000
		       0x0 0xae000000 0x0 0x30000000>;
	};

	host1x@50000000 {
		hdmi@54280000 {
			status = "okay";

			hdmi-supply = <&vdd_5v0_hdmi>;
			pll-supply = <&vdd_hdmi_reg>;
			vdd-supply = <&ldoln_reg>;

			nvidia,ddc-i2c-bus = <&hdmi_ddc>;
			nvidia,hpd-gpio =
				<&gpio TEGRA_GPIO(N, 7) GPIO_ACTIVE_HIGH>;
		};

		dsi@54300000 {
			status = "okay";

			avdd-dsi-csi-supply = <&ldo5_reg>;

			panel@0 {
				compatible = "auo,b080uan01";
				reg = <0>;

				power-supply = <&vdd_lcd>;
				backlight = <&backlight>;
			};
		};
	};

	cec@70015000 {
		status = "okay";
	};

	gpu@57000000 {
		status = "okay";
		vdd-supply = <&vdd_gpu>;
	};

	serial@70006000 {
		status = "okay";
	};

	/* Fourth high speed UART, exposed via debug board usb */
	serial@70006300 {
		status = "okay";
	};

	pwm@7000a000 {
		status = "okay";
	};

	i2c@7000c000 {
		status = "okay";
		clock-frequency = <100000>;

		rt5639: audio-codec@1c {
			compatible = "realtek,rt5639";
			reg = <0x1c>;
			interrupt-parent = <&gpio>;
			interrupts = <TEGRA_GPIO(H, 4) IRQ_TYPE_EDGE_FALLING>;
			realtek,ldo1-en-gpios =
				<&gpio TEGRA_GPIO(R, 2) GPIO_ACTIVE_HIGH>;
		};
	};

	/* Expansion GEN2_I2C_* */
	i2c@7000c400 {
		status = "okay";
		clock-frequency = <100000>;

		bat: battery-gauge@36 {
			compatible = "maxim,max17048";
			reg = <0x36>;
			power-supplies = <&charger>;

			interrupt-parent = <&gpio>;
			interrupts = <TEGRA_GPIO(Q, 5) IRQ_TYPE_NONE>;

			status = "okay";
		};

		charger: charger@6b {
			compatible = "ti,bq24193";
			reg = <0x6b>;

			interrupt-parent = <&gpio>;
			interrupts = <TEGRA_GPIO(J, 0) IRQ_TYPE_NONE>;
			omit-battery-class;

			battery_charger: charger {
				regulator-name = "batt_regulator";
			};
		};
	};

	/* HDMI DDC */
	hdmi_ddc: i2c@7000c700 {
		status = "okay";
		clock-frequency = <100000>;
	};

	/* Expansion PWR_I2C_*, on-board components */
	i2c@7000d000 {
		status = "okay";
		clock-frequency = <400000>;

		palmas: pmic@58 {
			compatible = "ti,tps65913", "ti,palmas";
			reg = <0x58>;
			interrupts = <GIC_SPI 86 IRQ_TYPE_LEVEL_HIGH>;

			#interrupt-cells = <2>;
			interrupt-controller;

			ti,system-power-controller;

			palmas_gpio: gpio {
				compatible = "ti,palmas-gpio";
				gpio-controller;
				#gpio-cells = <2>;
			};

			pmic {
				compatible = "ti,tps65913-pmic", "ti,palmas-pmic";
				ldo1-in-supply = <&palmas_smps6_reg>;
				ldo5-in-supply = <&palmas_smps8_reg>;
				ldo9-in-supply = <&palmas_smps9_reg>;
				ldoln-in-supply = <&palmas_smps10_out2_reg>;
				ldousb-in-supply = <&palmas_smps10_out2_reg>;

				regulators {
					vdd_cpu: smps123 {
						regulator-name = "vdd-cpu";
						regulator-min-microvolt = <700000>;
						regulator-max-microvolt = <1400000>;
						regulator-always-on;
						regulator-boot-on;
					};

					vdd_gpu: smps45 {
						regulator-name = "vdd-gpu";
						regulator-min-microvolt = <700000>;
						regulator-max-microvolt = <1400000>;
						regulator-init-microvolt = <1000000>;
					};

					palmas_smps6_reg: smps6 {
						regulator-name = "vdd-ddr";
						regulator-always-on;
						regulator-boot-on;
					};

					smps7 {
						regulator-name = "vdd-core";
						regulator-min-microvolt = <700000>;
						regulator-max-microvolt = <1400000>;
						regulator-always-on;
						regulator-boot-on;
					};

					palmas_smps8_reg: smps8 {
						regulator-name = "vdd-1v8";
						regulator-always-on;
						regulator-boot-on;
					};

					palmas_smps9_reg: smps9 {
						regulator-name = "vdd-snsr";
						regulator-min-microvolt = <3300000>;
						regulator-max-microvolt = <3300000>;
					};

					palmas_smps10_out1_reg: smps10_out1 {
						regulator-name = "vdd-out1-5v0";
						regulator-min-microvolt = <5000000>;
						regulator-max-microvolt = <5000000>;
					};

					palmas_smps10_out2_reg: smps10_out2 {
						regulator-name = "vdd-out2-5v0";
						regulator-min-microvolt = <5000000>;
						regulator-max-microvolt = <5000000>;
					};

					ldo1 {
						regulator-name = "avdd-pll";
						regulator-always-on;
						regulator-boot-on;
					};

					ldo2 {
						regulator-name = "vdd-cam";
						regulator-min-microvolt = <1050000>;
						regulator-max-microvolt = <1200000>;
					};

					ldo3 {
						regulator-name = "vdd-touch";
						regulator-min-microvolt = <3300000>;
						regulator-max-microvolt = <3300000>;
					};

					ldo4 {
						regulator-name = "avdd-cam";
						regulator-min-microvolt = <2700000>;
						regulator-max-microvolt = <2700000>;
					};

					ldo5_reg: ldo5 {
						regulator-name = "avdd-dsi-csi";
					};

					ldo6 {
						regulator-name = "vdd-cam-af";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <1800000>;
					};

					ldo7 {
						regulator-name = "avdd-cam-af";
						regulator-min-microvolt = <2700000>;
						regulator-max-microvolt = <2700000>;
					};

					ldo8 {
						regulator-name = "vdd-rtc";
						regulator-min-microvolt = <950000>;
						regulator-max-microvolt = <950000>;
						regulator-always-on;
						regulator-boot-on;
						ti,enable-ldo8-tracking;
					};

					vddio_sdmmc2: ldo9 {
						regulator-name = "vddio-sdmmc2";
						regulator-min-microvolt = <1800000>;
						regulator-max-microvolt = <3300000>;
					};

					ldoln_reg: ldoln {
						regulator-name = "vddio-hv";
						regulator-always-on;
						regulator-boot-on;
					};

					ldousb_reg: ldousb {
						regulator-name = "avdd-usb";
						regulator-always-on;
						regulator-boot-on;
					};
				};
			};

			rtc {
				compatible = "ti,palmas-rtc";
				interrupt-parent = <&palmas>;
				interrupts = <8 0>;
			};

		};
	};

	hda@70030000 {
		status = "okay";
	};

	usb@70090000 {
		phys = <&{/padctl@7009f000/pads/usb2/lanes/usb2-0}>; /* Micro A/B */
		phy-names = "usb2-0";

		avddio-pex-supply = <&palmas_smps10_out2_reg>;
		dvddio-pex-supply = <&palmas_smps10_out2_reg>;
		avdd-usb-supply = <&ldousb_reg>;
		avdd-pll-utmip-supply = <&palmas_smps8_reg>;
		avdd-pll-erefe-supply = <&ldo5_reg>;
		avdd-usb-ss-pll-supply = <&palmas_smps10_out2_reg>;
		hvdd-usb-ss-supply = <&ldousb_reg>;
		hvdd-usb-ss-pll-e-supply = <&ldousb_reg>;

		status = "okay";
	};

	padctl@7009f000 {
		status = "okay";

		avdd-pll-utmip-supply = <&palmas_smps8_reg>;
		avdd-pll-erefe-supply = <&ldo5_reg>;
		avdd-pex-pll-supply = <&palmas_smps10_out2_reg>;
		hvdd-pex-pll-e-supply = <&ldousb_reg>;

		pads {
			usb2 {
				status = "okay";

				lanes {
					usb2-0 {
						status = "okay";
						nvidia,function = "snps";
					};
				};
			};

			pcie {
				status = "okay";

				lanes {
					pcie-0 {
						status = "okay";
						nvidia,function = "usb3-ss";
					};
				};
			};
		};

		ports {
			/* Micro A/B */
			usb2-0 {
				status = "okay";
				mode = "otg";
				usb-role-switch;
				vbus-supply = <&battery_charger>;
			};
		};
	};

	/* SD card */
	mmc@700b0400 {
	};

	/* eMMC */
	mmc@700b0600 {
		status = "okay";
		bus-width = <8>;
		non-removable;
	};

	/* CPU DFLL clock */
	clock@70110000 {
		status = "okay";
		vdd-cpu-supply = <&vdd_cpu>;
		nvidia,i2c-fs-rate = <400000>;
	};

	usb@7d000000 {
		compatible = "nvidia,tegra124-udc";
		status = "okay";
		dr_mode = "peripheral";
	};

	usb-phy@7d000000 {
		status = "okay";
	};

	backlight: backlight {
		compatible = "pwm-backlight";
		pwms = <&pwm 1 40000>;

		brightness-levels = <0 4 8 16 32 64 128 255>;
		default-brightness-level = <6>;

		power-supply = <&lcd_bl_en>;
		enable-gpios = <&gpio TEGRA_GPIO(H, 1) GPIO_ACTIVE_HIGH>;
	};

	clk32k_in: clock-32k {
		compatible = "fixed-clock";
		clock-frequency = <32768>;
		#clock-cells = <0>;
	};

	cpus {
		cpu@0 {
			vdd-cpu-supply = <&vdd_cpu>;
			enable-method = "psci";
		};

		cpu@1 {
			enable-method = "psci";
		};

		cpu@2 {
			enable-method = "psci";
		};

		cpu@3 {
			enable-method = "psci";
		};
	};

	gpio-keys {
		compatible = "gpio-keys";

		key-power {
			label = "Power";
			gpios = <&gpio TEGRA_GPIO(Q, 0) GPIO_ACTIVE_LOW>;
			linux,code = <KEY_POWER>;
			debounce-interval = <10>;
			wakeup-source;
		};

		key-volume-down {
			label = "Volume Down";
			gpios = <&gpio TEGRA_GPIO(Q, 7) GPIO_ACTIVE_LOW>;
			linux,code = <KEY_VOLUMEDOWN>;
		};

		key-volume-up {
			label = "Volume Up";
			gpios = <&gpio TEGRA_GPIO(Q, 6) GPIO_ACTIVE_LOW>;
			linux,code = <KEY_VOLUMEUP>;
		};
	};

	psci {
		compatible = "arm,psci-0.2";
		method = "smc";
	};

	vdd_ac_bat_reg: regulator-acbat {
		compatible = "regulator-fixed";
		regulator-name = "vdd_ac_bat";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
	};

	lcd_bl_en: regulator-lcden {
		compatible = "regulator-fixed";
		regulator-name = "lcd_bl_en";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-boot-on;
		gpio = <&gpio TEGRA_GPIO(H, 2) GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};

	vddio_sdmmc3: regulator-sdmmc3 {
		compatible = "regulator-fixed";
		regulator-name = "vddio_sdmmc3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = <&gpio TEGRA_GPIO(R, 0) GPIO_ACTIVE_HIGH>;
		enable-active-high;
		vin-supply = <&palmas_smps9_reg>;
	};

	vdd_lcd: regulator-lcd {
		compatible = "regulator-fixed";
		regulator-name = "vdd_lcd";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		gpio = <&palmas_gpio 6 GPIO_ACTIVE_HIGH>;
		enable-active-high;
		vin-supply = <&palmas_smps8_reg>;
	};

	vdd_cam_reg: regulator-cam {
		compatible = "regulator-fixed";
		regulator-name = "vdd_cam";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		gpio = <&palmas_gpio 4 GPIO_ACTIVE_HIGH>;
		enable-active-high;
		vin-supply = <&palmas_smps8_reg>;
	};

	vdd_hdmi_reg: regulator-hdmi {
		compatible = "regulator-fixed";
		regulator-name = "vdd_hdmi";
		regulator-min-microvolt = <1200000>;
		regulator-max-microvolt = <1200000>;
		gpio = <&gpio TEGRA_GPIO(N, 5) GPIO_ACTIVE_HIGH>;
		enable-active-high;
		vin-supply = <&palmas_smps6_reg>;
	};

	vdd_5v0_hdmi: regulator-hdmicon {
		compatible = "regulator-fixed";
		regulator-name = "vdd_5v0_hdmi";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&gpio TEGRA_GPIO(K, 6) GPIO_ACTIVE_HIGH>;
		enable-active-high;
		vin-supply = <&palmas_smps10_out1_reg>;
	};

	sound {
	};

	thermal-zones {
	};
};
